class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_post, only: %i[ show edit update destroy ]

  # GET /posts or /posts.json
  def index
    @posts = Post.all.order('created_at DESC')
  end

  # GET /posts/1 or /posts/1.json
  def show
  end

  # GET /posts/new
  def new
    @post = Post.new
    status = Post.last.status
    if status > 4
      @current_status = 1
    else
      @current_status = (status + 1)
    end
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts or /posts.json
  def create
    @post = Post.new(post_params)

    SetVip.create_set(@post)
    SetChange.create_set_change(@post)
    SetChangePersen.create_set_change_persen(@post)
    SetMax.create_set_max(@post)
    SetMin.create_set_min(@post)
    SetValue.create_set_value(@post)

    respond_to do |format|
      if @post.save
        format.html { redirect_to posts_url, notice: "ออกหวยประจำวันที่ #{@post.created_at.strftime('%d/%m/%y')} เรียบร้อยแล้ว" }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1 or /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to posts_url, notice: "แก้ไขหวยประจำวันที่ #{@post.created_at.strftime('%d/%m/%y')} เรียบร้อยแล้ว" }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1 or /posts/1.json
  def destroy
    @post.destroy

    respond_to do |format|
      format.html { redirect_to posts_url, notice: "ลบหวยประจำวันที่ #{@post.created_at.strftime('%d/%m/%y')} เรียบร้อยแล้ว" }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def post_params
      params.require(:post).permit(:top, :bottom, :three, :note, :status)
    end
end
