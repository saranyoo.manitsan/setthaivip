class HomeController < ApplicationController
  def index
    @post_day = Post.where(created_at: Date.today.all_day)
    @post_day1 = @post_day.find_by(status: 1)
    @post_day2 = @post_day.find_by(status: 2)
    @post_day3 = @post_day.find_by(status: 3)
    @post_day4 = @post_day.find_by(status: 4)
    @posts = Post.all.limit(5).order('created_at DESC').where.not(created_at: Date.today.all_day)
    @post_last = Post.last
    @sets = @post_last.set_vip
    @set_changes = @post_last.set_change
    @set_change_persens = @post_last.set_change_persen
    @set_maxs = @post_last.set_max
    @set_mins = @post_last.set_min
    @set_values = @post_last.set_value
    if @post_last.present?
      if Date.today.strftime('%d/%m/%y') == @post_last.created_at.to_date.strftime('%d/%m/%y')
        @last =  @post_last
      end
    end
  end
end
