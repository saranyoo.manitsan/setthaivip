class SetChangePersen < ApplicationRecord
  belongs_to :post

  def self.create_set_change_persen(post)
    set = SetChangePersen.new
    set.set = rand(-5..5).to_s + '.'+ rand(11..99).to_s
    set.set_50 = rand(-5..5).to_s + '.'+ rand(11..99).to_s
    set.set_100 = rand(-5..5).to_s + '.' + rand(11..99).to_s
    set.s_set = rand(-5..5).to_s + '.' + rand(11..99).to_s
    set.set_clmv = rand(-5..5).to_s + '.' + rand(11..99).to_s
    set.set_hd = rand(-5..5).to_s + '.' + rand(11..99).to_s
    set.set_thsi = rand(-5..5).to_s + '.' + rand(11..99).to_s
    set.set_wb = rand(-5..5).to_s + '.' + rand(11..99).to_s
    set.set_mai = rand(-5..5).to_s + '.' + rand(11..99).to_s
    set.post = post
    set.save
  end
end
