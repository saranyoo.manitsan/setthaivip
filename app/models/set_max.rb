class SetMax < ApplicationRecord
  belongs_to :post

  def self.create_set_max(post)
    set = SetMax.new
    set.set = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.set_50 = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.set_100 = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.s_set = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.set_clmv = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.set_hd = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.set_thsi = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.set_wb = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.set_mai = rand(1..15).to_s + '.' + rand(11..99).to_s
    set.post = post
    set.save
  end
end
