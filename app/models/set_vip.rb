class SetVip < ApplicationRecord
  belongs_to :post

  def self.create_set(post)
    set = SetVip.new
    set.set = rand(1000..3000).to_s + '.'+ post.top
    set.set_50 = rand(1000..3000).to_s + '.'+ rand(1..9).to_s + post.three
    set.set_100 = rand(1000..3000).to_s + '.' + rand(11..99).to_s
    set.s_set = rand(1000..3000).to_s + '.' + rand(11..99).to_s
    set.set_clmv = rand(1000..3000).to_s + '.' + rand(11..99).to_s
    set.set_hd = rand(1000..3000).to_s + '.' + rand(11..99).to_s
    set.set_thsi = rand(1000..3000).to_s + '.' + rand(11..99).to_s
    set.set_wb = rand(1000..3000).to_s + '.' + rand(11..99).to_s
    set.set_mai = rand(1000..3000).to_s + '.' + rand(11..99).to_s
    set.post = post
    set.save
  end
end
