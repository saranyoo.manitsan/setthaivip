class SetChange < ApplicationRecord
  belongs_to :post

  def self.create_set_change(post)
    set = SetChange.new
    set.set = rand(-10..10).to_s + '.'+ post.bottom
    set.set_50 = rand(-10..10).to_s + '.'+ rand(11..99).to_s
    set.set_100 = rand(-10..10).to_s + '.' + rand(11..99).to_s
    set.s_set = rand(-10..10).to_s + '.' + rand(11..99).to_s
    set.set_clmv = rand(-10..10).to_s + '.' + rand(11..99).to_s
    set.set_hd = rand(-10..10).to_s + '.' + rand(11..99).to_s
    set.set_thsi = rand(-10..10).to_s + '.' + rand(11..99).to_s
    set.set_wb = rand(-10..10).to_s + '.' + rand(11..99).to_s
    set.set_mai = rand(-10..10).to_s + '.' + rand(11..99).to_s
    set.post = post
    set.save
  end
end
