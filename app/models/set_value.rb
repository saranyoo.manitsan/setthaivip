class SetValue < ApplicationRecord
  belongs_to :post

  def self.create_set_value(post)
    set = SetValue.new
    set.set = rand(1000000..30000000).to_s
    set.set_50 = rand(1000000..30000000).to_s
    set.set_100 = rand(1000000..30000000).to_s
    set.s_set = rand(1000000..30000000).to_s
    set.set_clmv = rand(1000000..30000000).to_s
    set.set_hd = rand(1000000..30000000).to_s
    set.set_thsi = rand(1000000..30000000).to_s
    set.set_wb = rand(1000000..30000000).to_s
    set.set_mai = rand(1000000..30000000).to_s
    set.post = post
    set.save
  end
end
