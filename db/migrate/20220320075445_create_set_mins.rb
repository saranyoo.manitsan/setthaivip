class CreateSetMins < ActiveRecord::Migration[6.0]
  def change
    create_table :set_mins do |t|
      t.string :set
      t.string :set_50
      t.string :set_100
      t.string :s_set
      t.string :set_clmv
      t.string :set_hd
      t.string :set_thsi
      t.string :set_wb
      t.string :set_mai
      t.belongs_to :post

      t.timestamps
    end
  end
end
