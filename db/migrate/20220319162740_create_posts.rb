class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.string :top
      t.string :bottom
      t.string :three
      t.string :note

      t.timestamps
    end
  end
end
