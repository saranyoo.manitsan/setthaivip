# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_03_20_151427) do

  create_table "posts", force: :cascade do |t|
    t.string "top"
    t.string "bottom"
    t.string "three"
    t.string "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status"
  end

  create_table "set_amounts", force: :cascade do |t|
    t.string "set"
    t.string "set_50"
    t.string "set_100"
    t.string "s_set"
    t.string "set_clmv"
    t.string "set_hd"
    t.string "set_thsi"
    t.string "set_wb"
    t.string "set_mai"
    t.integer "post_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_id"], name: "index_set_amounts_on_post_id"
  end

  create_table "set_change_persens", force: :cascade do |t|
    t.string "set"
    t.string "set_50"
    t.string "set_100"
    t.string "s_set"
    t.string "set_clmv"
    t.string "set_hd"
    t.string "set_thsi"
    t.string "set_wb"
    t.string "set_mai"
    t.integer "post_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_id"], name: "index_set_change_persens_on_post_id"
  end

  create_table "set_changes", force: :cascade do |t|
    t.string "set"
    t.string "set_50"
    t.string "set_100"
    t.string "s_set"
    t.string "set_clmv"
    t.string "set_hd"
    t.string "set_thsi"
    t.string "set_wb"
    t.string "set_mai"
    t.integer "post_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_id"], name: "index_set_changes_on_post_id"
  end

  create_table "set_maxes", force: :cascade do |t|
    t.string "set"
    t.string "set_50"
    t.string "set_100"
    t.string "s_set"
    t.string "set_clmv"
    t.string "set_hd"
    t.string "set_thsi"
    t.string "set_wb"
    t.string "set_mai"
    t.integer "post_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_id"], name: "index_set_maxes_on_post_id"
  end

  create_table "set_mins", force: :cascade do |t|
    t.string "set"
    t.string "set_50"
    t.string "set_100"
    t.string "s_set"
    t.string "set_clmv"
    t.string "set_hd"
    t.string "set_thsi"
    t.string "set_wb"
    t.string "set_mai"
    t.integer "post_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_id"], name: "index_set_mins_on_post_id"
  end

  create_table "set_values", force: :cascade do |t|
    t.string "set"
    t.string "set_50"
    t.string "set_100"
    t.string "s_set"
    t.string "set_clmv"
    t.string "set_hd"
    t.string "set_thsi"
    t.string "set_wb"
    t.string "set_mai"
    t.integer "post_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_id"], name: "index_set_values_on_post_id"
  end

  create_table "set_vips", force: :cascade do |t|
    t.string "set"
    t.string "set_50"
    t.string "set_100"
    t.string "s_set"
    t.string "set_clmv"
    t.string "set_hd"
    t.string "set_thsi"
    t.string "set_wb"
    t.string "set_mai"
    t.integer "post_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["post_id"], name: "index_set_vips_on_post_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
