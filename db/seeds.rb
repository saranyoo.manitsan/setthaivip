# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)



for a in 1..20 do
  pp1 = Post.new(top: rand(11..99).to_s, bottom: rand(11..99).to_s, three: rand(0-9).to_s, status: '1')
  pp1.created_at = DateTime.now-a
  pp1.save
  puts pp1.created_at.to_s + 'saved'

  pp2 = Post.new(top: rand(11..99).to_s, bottom: rand(11..99).to_s, three: rand(0-9).to_s, status: '2')
  pp2.created_at = DateTime.now-a
  pp2.save
  puts pp2.created_at.to_s + 'saved'

  pp3 = Post.new(top: rand(11..99).to_s, bottom: rand(11..99).to_s, three: rand(0-9).to_s, status: '3')
  pp3.created_at = DateTime.now-a
  pp3.save
  puts pp3.created_at.to_s + 'saved'

  pp4 = Post.new(top: rand(11..99).to_s, bottom: rand(11..99).to_s, three: rand(0-9).to_s, status: '4')
  pp4.created_at = DateTime.now-a
  pp4.save
  puts pp4.created_at.to_s + 'saved'
end
